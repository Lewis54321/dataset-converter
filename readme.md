# DATASET CONVERTER

## OVERVIEW

Gui developed for converting datasets between different datatypes. Datatypes currently supported:

- CSV
- Pickle
- Parquet
- MySQL Database

## GENERAL DEPENDENCIES

- Python
- MySQL Server

## LIBRARY DEPENDENCIES

- PyQt5 (version 5.9.2 tested)
- sqlalchemy (version 1.3.16 tested)
- numpy (version 1.18.1 tested)
- pandas (version 1.0.1 tested)
- mysql-connector-python (version 8.0.18 tested)

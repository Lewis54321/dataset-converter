import numpy as np
import pandas as pd


class Import:

    def __init__(self):
        pass

    @staticmethod
    def import_mysql(tableName, schemaName, password, update, dataReady):
        """
        Worker for importing table from the mySQL database
        :param tableName: String [Name of the table to be imported from]
        :param schemaName: String [Name of the schema to be imported from]
        :param password: String [Database password]
        :param update
        :param dataReady
        :return: threadFinished
        """

        # Set up the connection object
        engine = create_engine('mysql+pymysql://root:%s@localhost/%s' % (password, schemaName), pool_recycle=3600)
        dbConnection = engine.connect()
        update.emit('Successfully connected to the MySQL database', 'import')
        dataset = pd.read_sql('SELECT * FROM %s.%s' % (schemaName, tableName), dbConnection, index_col=None)
        dataReady.emit(dataset)

        return 'import'

    @staticmethod
    def import_csv(directory, numCol, numRow, update, dataReady):
        """
        Method for importing csv file
        :param directory: string
        :param numCol: int
        :param numRow: int
        :param update
        :return: threadFinished
        """

        colLevels = list(range(numCol))
        rowLevels = list(range(numRow))
        if numRow == 0:
            rowLevels = [0]
        if numRow == 0 and numCol == 1:
            colLevels = 0
            rowLevels = False
        dataset = pd.read_csv(directory, header=colLevels, index_col=rowLevels)
        dataReady.emit(dataset)
        return 'import'

    @staticmethod
    def import_pickle(directory, update, dataReady):
        """
        Method for importing pickle file
        :param directory: string
        :param update:
        :param dataReady:
        :return:
        """

        dataset = pd.read_pickle(directory)
        if not isinstance(dataset, pd.DataFrame):
            raise Exception('Error importing pickle datafile. Pickle data not DataFrame type')
        update.emit('Successfully imported dataset from data file', 'import')
        dataReady.emit(dataset)
        return 'import'

    @staticmethod
    def import_parquet(directory, update, dataReady):
        """
        Method for importing parquet file
        :param directory: string
        :param update:
        :param dataReady:
        :return:
        """

        dataset = pd.read_parquet(directory)
        if not isinstance(dataset, pd.DataFrame):
            raise Exception('Error importing pickle datafile. Pickle data not DataFrame type')
        update.emit('Successfully imported dataset from data file', 'import')
        dataReady.emit(dataset)
        return 'import'

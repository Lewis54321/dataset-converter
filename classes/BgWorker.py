from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from PyQt5.QtCore import *

import traceback
import pandas as pd


class WorkerClass(QObject):

    update = pyqtSignal(str, str)
    dataReady = pyqtSignal(pd.DataFrame)
    finished = pyqtSignal(str)
    error = pyqtSignal(tuple)

    def __init__(self, fn, *args, **nargs):

        super(self.__class__, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.nargs = nargs

    @pyqtSlot()
    def run_function(self):

        try:
            stage = self.fn(*self.args, **self.nargs, update=self.update, dataReady=self.dataReady)
            self.finished.emit(stage)

        except BaseException as exception:
            traceback.print_exc()
            self.error.emit(exception)

import numpy as np
import pandas as pd
import os


class Export:

    def __init__(self):
        pass

    @staticmethod
    def export_mysql(dataset, tableName, schemaName, password, update, dataReady):
        """
        Worker for exporting data to the mySQL database
        :param dataset: pd.DataFrame
        :param tableName: string
        :param schemaName: string
        :param password: string
        :return threadFinished: string
        """

        # Set up the connection object
        engine = create_engine('mysql+pymysql://root:%s@localhost/%s' % (password, schemaName), pool_recycle=3600)
        dbConnection = engine.connect()
        update.emit('Successfully connected to MySQL database')
        dataset.to_sql(tableName, dbConnection, chunksize=50000, if_exists='fail', index=False)

        return 'export'

    @staticmethod
    def export_csv(dataset, directory, update, dataReady):
        """
        Method for importing csv file
        :param dataset: pd.DataFrame
        :param directory: string
        :param update
        :param dataReady
        :return threadFinished: string
        """

        dataset.to_csv(directory)
        update.emit('Successfully exported dataset to csv file', 'export')
        return 'export'

    @staticmethod
    def export_pickle(dataset, directory, update, dataReady):
        """
        Method for importing pickle file
        :param dataset: pd.DataFrame
        :param directory: string
        :param update:
        :param dataReady:
        :return threadFinished: string
        """

        dataset.to_pickle(directory + '.pickle')
        update.emit('Successfully exported dataset to pickle file', 'export')
        return 'export'

    @staticmethod
    def export_parquet(dataset, directory, update, dataReady):
        """
        Method for importing parquet file
        :param directory: string
        :param update:
        :param dataReady:
        :return:
        """

        dataset.to_parquet(directory + '.parquet')
        update.emit('Successfully exported dataset to parquet file', 'export')
        return 'export'

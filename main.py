from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QFileDialog

from window import Ui_MainWindow
from sqlalchemy import create_engine
import sys
import mysql.connector
import os
import datetime
import pickle
from functools import partial

from classes.BgWorker import WorkerClass
from classes.Import import Import
from classes.Export import Export


def catch_exceptions(t, val, tb):
    QtWidgets.QMessageBox.critical(None, "An exception was raised", "Exception type: {}".format(t))
    old_hook(t, val, tb)

# Handles exceptions for debugging
old_hook = sys.excepthook
sys.excepthook = catch_exceptions


class Main(QtWidgets.QMainWindow):

    def __init__(self):

        QtWidgets.QMainWindow.__init__(self)
        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)

        # Set up signals
        self.set_up_signals()

        # Gets the current working directory
        self._cwd = os.getcwd()

        # Get the current date and time
        self._dateTime = datetime.datetime.now().strftime("%Y%m%d_%H%M")

        # Setting up the background thread and background worker
        self._thread = QThread()
        self._worker = None

        # Import Directories
        self._pickleDir = None
        self._textDir = None

        # Main data containers
        self._dataset = None
        self._filename = None

        self._importWorker = None

    def set_up_signals(self):
        """
        Method for setting up all the widget signals when the form loads
        """

        def dataset_type_imp(dataType):
            """
            Combobox triggered method for enabling groupbox depending of dataset to import
            :param dataType: string
            """
            self._ui.groupBoxSqlImp.setDisabled(True)
            self._ui.groupBoxCsvImp.setDisabled(True)
            self._ui.groupBoxDirImp.setDisabled(True)
            if dataType == 'CSV' or dataType == 'Pickle' or dataType == 'Parquet':
                self._ui.groupBoxDirImp.setEnabled(True)
            if dataType == 'CSV':
                self._ui.groupBoxCsvImp.setEnabled(True)
            if dataType == 'MySQL':
                self._ui.groupBoxSqlImp.setEnabled(True)

        def dataset_type_exp(dataType):
            """
            Combobox triggered method for enabling groupbox depending of dataset to export
            :param dataType: string
            """
            self._ui.groupBoxSqlExp.setDisabled(True)
            self._ui.groupBoxDirExp.setDisabled(True)
            if dataType == 'CSV' or dataType == 'Pickle' or dataType == 'Parquet':
                self._ui.groupBoxDirExp.setEnabled(True)
            if dataType == 'MySQL':
                self._ui.groupBoxSqlExp.setEnabled(True)

        # IMPORT
        self._ui.comboBoxTypeImp.currentTextChanged.connect(dataset_type_imp)
        self._ui.pushButtonSqlImp.clicked.connect(self.get_import_data)
        self._ui.pushButtonSelectDirImp.clicked.connect(partial(self.get_dir, 'import'))
        self._ui.pushButtonFileImp.clicked.connect(self.get_import_data)

        # EXPORT
        self._ui.comboBoxTypeExp.currentTextChanged.connect(dataset_type_exp)
        self._ui.pushButtonSqlExp.clicked.connect(self.get_export_data)
        self._ui.pushButtonSelectDirExp.clicked.connect(partial(self.get_dir, 'export'))
        self._ui.pushButtonFileExp.clicked.connect(self.get_export_data)

    # IMPORT
    @pyqtSlot()
    def get_import_data(self):
        """
        Button triggered method for importing the database
        """

        dataType = self._ui.comboBoxTypeImp.currentText()
        if dataType == 'MySQL':
            self._worker = WorkerClass(Import.import_mysql, self._ui.lineEditTableNameImp.text(),
                                       self._ui.lineEditSchemaNameImp.text(), self._ui.lineEditPasswordImp.text())
        elif dataType == 'CSV':
            self._worker = WorkerClass(Import.import_csv, self._ui.lineEditDirImp.text(),
                                       int(self._ui.comboBoxCsvColImp.currentText()),
                                       int(self._ui.comboBoxCsvRowImp.currentText()))
        elif dataType == 'Pickle':
            self._worker = WorkerClass(Import.import_pickle, self._ui.lineEditDirImp.text())
        elif dataType == 'Parquet':
            self._worker = WorkerClass(Import.import_parquet, self._ui.lineEditDirImp.text())

        self._worker.update.connect(self.update_message)
        self._worker.dataReady.connect(self.data_ready)
        self._worker.finished.connect(self.thread_finished)
        self._worker.moveToThread(self._thread)
        self._thread.started.connect(self._worker.run_function)
        self._thread.start()
        self._ui.progressBarImp.setValue(0)

        self.update_message('Importing Data...', 'import')

    # EXPORT
    @pyqtSlot()
    def get_export_data(self):
        """
        Button triggered Method for exporting the pickle feature DataFrame, most work performed on background thread by
        passing the "exportFeature" method
        """

        if self._ui.checkBoxOptMemory.isChecked():
            self._dataset = self.optimise_data(self._dataset)

        dataType = self._ui.comboBoxTypeExp.currentText()
        if dataType == 'MySQL':
            self._worker = WorkerClass(Export.export_mysql, self._dataset, self._ui.lineEditTableNameExp.text(),
                                       self._ui.lineEditSchemaNameExp.text(), self._ui.lineEditPasswordExp.text())
        elif dataType == 'CSV':
            self._worker = WorkerClass(Export.export_csv, self._dataset, self._ui.lineEditDirExp.text())
        elif dataType == 'Pickle':
            self._worker = WorkerClass(Export.export_pickle, self._dataset,
                                       os.path.join(self._ui.lineEditDirExp.text(), self._filename))
        elif dataType == 'Parquet':
            self._worker = WorkerClass(Export.export_parquet, self._dataset,
                                       os.path.join(self._ui.lineEditDirExp.text(), self._filename))

        self._worker.update.connect(self.update_message)
        self._worker.dataReady.connect(self.data_ready)
        self._worker.finished.connect(self.thread_finished)
        self._worker.moveToThread(self._thread)
        self._thread.started.connect(self._worker.run_function)
        self._thread.start()
        self._ui.progressBarExp.setValue(0)

        self.update_message('Exporting Data...', 'export')

    @staticmethod
    def optimise_data(dataset):
        """
        Method for lowering the memory requirements of the dataset
        :param dataset: pd.DataFrame
        :return dataset: pd.DataFrame
        """

        for col in dataset.columns:
            if dataset[col].dtype == 'float64':
                dataset[col] = dataset[col].astype('float32')
            if dataset[col].dtype == 'int64':
                dataset[col] = dataset[col].astype('int32')
        return dataset

    # DIRECTORY MANAGEMENT
    @pyqtSlot()
    def get_dir(self, stage):
        """
        Button triggered method for selecting the directory
        """

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setNameFilter("Data File (*.csv *.pickle *.parquet)")

        if dlg.exec_():
            dataDir = dlg.selectedFiles()[0]

        try:
            if stage == 'import':
                self._ui.lineEditDirImp.setText(dataDir)
            if stage == 'export':
                self._ui.lineEditDirExp.setText(dataDir)
            self._filename = os.path.basename(os.path.splitext(dataDir)[0])
            self._ui.statusbar.showMessage('Directory Selected')
        except BaseException as exception:
            self._ui.statusbar.showMessage('Could not update directory')
            raise exception

    # EVENTS
    def update_message(self, message, stage):

        self._ui.statusbar.showMessage(message)
        if stage == 'import':
            self._ui.listWidgetDescImp.addItem(message)
        elif stage == 'export':
            self._ui.listWidgetDescExp.addItem(message)

    def data_ready(self, data):
        """
        Import dataset
        :param data: pd.DataFrame
        """

        self._dataset = data

    def thread_finished(self, stage):
        """
        Thread Finished method called by the background thread to indicate that the dataset has been imported
        :param stage: String [Stage of processing, i.e.
        :param message:
        """

        def update_import_descriptor():

            self._ui.listWidgetDescImp.addItem('Number of Rows: %d' % (self._dataset.shape[0]))
            self._ui.listWidgetDescImp.addItem('Number of Columns: %d' % (self._dataset.shape[1]))

        if stage == 'import':
            update_import_descriptor()
            self._ui.tabExport.setEnabled(True)
            self._ui.lineEditDirExp.setText(os.path.join(self._cwd, 'export'))
            self.update_message('Import Complete', 'import')
            self._ui.progressBarImp.setValue(100)
        elif stage == 'export':
            self.update_message('Export Complete', 'export')
            self._ui.progressBarExp.setValue(100)

        self._thread.quit()


if __name__ == "__main__":

    # Defines a new application process
    app = QtWidgets.QApplication(sys.argv)

    # Create new UI MainWindow class object and assign the window widget to it
    mainWindow = Main()
    mainWindow.show()
    sys.exit(app.exec_())


